const path = require('path');

const apiClient = process.env.VUE_APP_API_CLIENT;
module.exports = {
  configureWebpack: (config) => {
    config.entry =  './src/main.js';

    config.resolve = {
      extensions: [
        '.js',
        '.vue',
        '.json'
      ],
        alias: {
        'vue$': 'vue/dist/vue.esm.js',
          '@': path.resolve(__dirname, 'src'),
          'api-client': path.resolve(__dirname, `src/api/${apiClient}`)
      }
    };
    console.log(path.resolve(__dirname, 'src'));
    config.devServer = {
      compress: true,
        host: 'localhost',
        disableHostCheck: true,
        https: false,
        open: true,
        port: 9000
    };

    config.optimization = {
      splitChunks: {
        chunks: 'all',
          minSize: 30000,
          maxSize: 0,
          cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
              priority: -10
          },
        default: {
            minChunks: 2,
              priority: -20,
              reuseExistingChunk: true
          }
        }
      },
      runtimeChunk: {
        name: entrypoint => `runtime~${entrypoint.name}`
      },
      mangleWasmImports: true,
        removeAvailableModules: true,
        removeEmptyChunks: true,
        mergeDuplicateChunks: true
    };
  },
};
