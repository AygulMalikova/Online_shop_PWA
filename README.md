# Inference technologies 
## Test task

- [Project setup](#project-setup)
- [Test in browser in desktop](#test-in-browser-in-desktop)
- [Test in browser on mobile phone](#test-in-browser-on-mobile-phone)
- [Test as PWA on mobile phone](#test-as-pwa-on-mobile-phone)
- [Unit test with Jest](#unit-test-with-jest)
- [Stack of used technologies](#stack-of-used-technologies)

# Project setup

Download the project and run
```
yarn install
```

# Test in browser in desktop

- Run this command to compile and minify for production
```
yarn run build && serve dist
```

- Open localhost on corresponded port you will see the first page.
<img src="./public/img/1.png" height="300px">


- Click on Start Scanning button and show on camera one of barcodes that are located at this [directory](./src/api/mock/data/barcodes)

- You may change the amount of scanned product in the scanner page as well as in cart page.

# Test in browser on mobile phone
In order to test on mobile device you need to have secure connection because of using user media screen. 
Without secure connection the browser will block the camera. 
To get it without deploying on server you need to download [ngrok](https://ngrok.com/download) 
and once you have installed it run the following code from terminal
```
./ngrok http 5000
```

ngrok - name of downloaded file
5000 - port on which the add is running now

<img src="./public/img/2.jpg" height="400px">

Copy the link with https and open in your mobile browser

<img src="./public/img/3.jpg" height="400px">

<img src="./public/img/4.jpg" height="400px">

<img src="./public/img/5.jpg" height="400px">

# Test as PWA on mobile phone

In order to run it as PWA you should click on the button "Add to home screen" in the browser and APP will be added.
Unfortunately, there are some issues with userMedia in PWA in IOS. Because of that camera is not opening on iPhones during using the app as PWA. There is a
[Link](https://github.com/gruhn/vue-qrcode-reader/issues/76) where this issues are described. But the app is still working successfully in mobile browsers.

<img src="./public/img/6.jpg" height="400px">

# Unit test with Jest
In order to run unit test, run the following command:
```
yarn test
```
The coverage you may in /reports/index.html
For now, the components are checked only for rendering

# Stack of used technologies
- Vue, vue-router, vuex
- Sass/Scss
- quaggajs - library for scanning barcode written on JS
- webpack
- @vue/cli-plugin-pwa - for making app as PWA
- jest - for
