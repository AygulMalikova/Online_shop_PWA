// jest.config.js
module.exports = {
  roots: [
    './src'
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    "./src/components/**/*.{js,vue}",
    "!./src/api/**",
    "!./src/*.{js,vue}",
  ],
  coverageDirectory: './reports/coverage',
  coverageReporters: [
    'html'
  ],
  testPathIgnorePatterns: [
    '/node_modules/'
  ],
  moduleNameMapper: {
    '\\.(css|scss)': 'identity-obj-proxy',
    '\\.(svg|png|jpg|jpeg)': '/__mocks__/filemock.js',
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  testRegex: '(/__tests__/.*|(\\.|/))+(test|spec)\\.(js)?$',
  snapshotSerializers: ["jest-serializer-vue"],
  moduleFileExtensions: ["js", "json", "vue"],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": "vue-jest"
  }
};
