import Vue from 'vue'
import Router from 'vue-router'

// Page content
import Scanner from '@/components/Scanner'
import Cart from '@/components/Cart'


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/scanner'
    },
    {
      path: '/cart',
      name: 'Users cart',
      component: Cart
    },
    {
      path: '/scanner',
      name: 'Scanner',
      component: Scanner
    }
  ]
})
