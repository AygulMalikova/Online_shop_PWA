import Counter from '../components/Common/Counter';
import { shallowMount } from '@vue/test-utils';

describe('Counter', () => {
  it('render test', () => {
    const wrapper = shallowMount(Counter, {
      propsData: {
        value: 3
      }
    });
    expect(wrapper).toMatchSnapshot();
  });
});
