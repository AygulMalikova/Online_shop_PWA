import Table from '../components/Cart/Table';
import { shallowMount } from '@vue/test-utils';

describe('Table', () => {
  it('render test', () => {
    const wrapper = shallowMount(Table, {
      propsData: {
        productsInCart: [
          {
            product:
              {
                "id": 2,
                "code": "9780201379624",
                "name": "Snickers",
                "imageUrl": "https://amwine.ru/upload/iblock/6a0/6a08e068afcb289708eec67b4005a3f2.png"
              },
            amount: 2
          }
        ]
      }
    });
    expect(wrapper).toMatchSnapshot();
  });
});
