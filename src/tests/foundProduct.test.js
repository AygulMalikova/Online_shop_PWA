import FoundProduct from '../components/Scanner/FoundProduct';
import { shallowMount } from '@vue/test-utils';

describe('FoundProduct', () => {
  it('render test', () => {
    const wrapper = shallowMount(FoundProduct, {
      propsData: {
        product: {
          "id": 3,
          "code": "5901234123457",
          "name": "Milka chocolate",
          "imageUrl": "https://www.pngkit.com/png/full/54-547924_milka-chocolate-bar-300g-milka-oreo-fresh.png"
        },
        amountOfProduct: 4
      }
    });
    expect(wrapper).toMatchSnapshot();
  });
});
