import Button from '../components/Common/Button';
import { shallowMount } from '@vue/test-utils';

describe('Button', () => {
  it('render test', () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        content: 'Click!',
        disabled: false
      }
    });
    expect(wrapper).toMatchSnapshot();
  });
});
