import App from '../App.vue';
import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe('App', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(App, {
      localVue,
      router
    });
  });

  it('render test', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
