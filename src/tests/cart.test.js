import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import Cart from '../components/Cart';

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(VueRouter);

const router = new VueRouter();

describe('Cart', () => {
  let actions;
  let state;
  let store;
  beforeEach(() => {
    state = {
      cart: [
        {
          product:
            {
              "id": 2,
              "code": "9780201379624",
              "name": "Snickers",
              "imageUrl": "https://amwine.ru/upload/iblock/6a0/6a08e068afcb289708eec67b4005a3f2.png"
            },
          amount: 2
        }
      ]
    };

    actions = {
      updateAmountOfProductById: jest.fn()
    };

    store = new Vuex.Store({
      actions, state
    });
  });
  it('render test', () => {
    const wrapper = shallowMount(Cart, { store, router, localVue });
    expect(wrapper).toMatchSnapshot();
  });
});
