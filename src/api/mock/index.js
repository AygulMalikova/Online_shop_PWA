import products from './data/products';
import productById from './data/productById';

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData);
    }, time);
  });
};

export default {
  fetchProducts() {
    return fetch(products, 100);
  },
  fetchProductById(id) {
    const result = productById.find(item => item.id === id);
    return fetch(result, 100);
  }
};
