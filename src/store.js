import Vue from 'vue';
import Vuex from 'vuex';
import client from 'api-client';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    currentProduct: null,
    cart: []
  },

  mutations: {
    setProducts(state, products) {
      state.products = products;
    },
    setCurrentProduct(state, currentProduct) {
      state.currentProduct = currentProduct;
    },
    addToCart(state, {product, amount}) {
      state.cart.push({product, amount});
    },
    updateCart(state, {productId, amount}) {
      const arrayOfCarts = state.cart;
      const id = arrayOfCarts.findIndex((item) => {
        return item.product.id === productId;
      });
      arrayOfCarts[id]  = { ...arrayOfCarts[id], amount: amount};
      state.cart = arrayOfCarts;
    }
  },

  actions: {
    fetchProducts({ commit }) {
      return client
        .fetchProducts()
        .then(products => commit('setProducts', products));
    },
    fetchProduct({ commit }, id) {
      if (id != null) {
        return client
          .fetchProductById(id)
          .then(product => commit('setCurrentProduct', product));
      } else {
        commit('setCurrentProduct', null);
      }
    },

    addProductToCart({ commit, getters }, {product, amount}) {
      const productId = product.id;
      if (getters.getProductFromCart(productId)) {
        commit('updateCart', {productId, amount});
      }
      else {
        commit('addToCart', {product, amount});
      }

    },

    updateAmountOfProductById( {commit}, {productId, amount}) {
      commit('updateCart', {productId, amount});
    },
  },
  getters: {
    getProductFromCart: state => productId => {
      return state.cart.find(item => item.product.id === productId)
    }
  }
});
